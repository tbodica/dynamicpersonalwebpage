# React.js Redux CV webpage

The components in this React.js portfolio site include:
- A list of tabular items connected to a DB persistence microservice
- A list of strings (event descriptions) connected to a DB 
- A card with a profile pic and hyperlinks to other social media
- An aesthetic timeline to act as a CV (image below)

![Capture](Capture.JPG)